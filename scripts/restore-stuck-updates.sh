#!/bin/sh

### FILTERs
STUCK_WEBSITE_FILTER='.status.releaseID.current != .status.releaseID.failsafe'
WEBSITE_LIST_OUTPUT='(.metadata.name + " " + .metadata.namespace + " " + .status.releaseID.failsafe)'

# Time between patches not to overwhelm the Operator
# This can be skipped/reduced once we have this issue closed: https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/869
export GRACE_PERIOD="1s"

## NB: This will match the spec of all websites based on failStatus version
##
oc get -o json drupalsite -A |
    jq -r ".items[] | select(${STUCK_WEBSITE_FILTER}) | ${WEBSITE_LIST_OUTPUT}"  |
        # Retrieve Spec Name from failSafe status
        xargs -l bash -c 'export name=$(echo $2 | cut -d"-" -f -2);
        # Retrieve spec ReleaseID from failsafe status
        export spec=$(echo $2 |cut -d"-" -f 3"-"45);
        # Generate Patch to be applied
        cat > patch.yaml <<EOF
spec:
  version:
    name: ${name}
    releaseSpec: ${spec}
EOF
        oc patch drupalsite/$0 -n $1 --type=merge --patch-file patch.yaml;
        sleep ${GRACE_PERIOD}'
