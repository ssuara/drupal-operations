#!/bin/bash
# This script fixes issue https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/885
# It accesses the database of every website, iterates over a list of tables,
# and for each {website,table} it resets the auto_increment
# which broke after the first Drupal 9.3 upgrade.


PATH="$PATH:~/k8s-migrate/"

# 1. Get all the DB credentials
# Decode them
# And store them in a single line each in a file. Each line is a JSON for a single site.

oc get -o json secret -l dbcredentials -A | \
  jq -r '.items[] | .data[] |= @base64d | {"name": (.metadata.namespace + .metadata.name), "data": .data}' | \
  jq -r -c '{"name":.name} + {"data": (.data | to_entries | map(("export "+.key+"="+.value)))}' > secrets.json

# 2. Create a DB env file for each site

[[ ! -d "sites" ]] && mkdir "sites"

create_env_file(){
  JSON="$1"
  filename=$(jq -r '.name' <<<"$JSON")
  jq -r '.data[]' <<<"$JSON" > "sites/$filename"
}

while IFS= read -r siteJSON; do
  create_env_file "$siteJSON"
done < secrets.json

# 3. Perform the operation for each DB

TABLES_FILE="`dirname $0`/tables-primarykey"

sql_connect(){
  echo "mysql --user=$dbUser --password=$dbPassword --database=$dbName --host=$dbHost --port=$dbPort -A -s"
}
export -f sql_connect

reset_table(){
  TABLE="$0"
  COLUMN="$1"
  connstr="`sql_connect`"
  NEW_INCR=$(($(echo "SELECT max($COLUMN) from $TABLE;" | $connstr)+1))
  echo "ALTER TABLE $TABLE AUTO_INCREMENT = $NEW_INCR;" | $connstr
}
export -f reset_table

for site in `ls sites`; do
  echo "> $site" | tee -a "`basename $0`.log"
  . "sites/$site"
  cat "$TABLES_FILE" | xargs -L1 bash -c 'reset_table $1'
done
