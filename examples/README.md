### tekton-tasks

Examples of backing up or restoring Drupal sites.

### logs-hdfs

How to fetch site logs from long-term storage on HDFS.

This Jupyter notebook should be run on the SWAN service ([swan.cern.ch](https://swan.cern.ch))
using the SPARK plugin.
